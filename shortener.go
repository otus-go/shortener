package main

import (
	"fmt"
	"strings"
	"unicode/utf8"
)

var charmap = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")

// Reverse just reverse the string
func Reverse(s string) string {
	size := len(s)
	buf := make([]byte, size)
	for start := 0; start < size; {
		r, n := utf8.DecodeRuneInString(s[start:])
		start += n
		utf8.EncodeRune(buf[size-start:], r)
	}
	return string(buf)
}

// Shorten - return base62 representation of integer
func Shorten(id uint64) string {
	var b strings.Builder
	for id != 0 {
		fmt.Fprintf(&b, "%c", charmap[id%62])
		id /= 62
	}
	return Reverse(b.String())
}

// Resolve - convert base62 string into integer
func Resolve(shortened string) uint64 {
	var n uint64
	for _, r := range shortened {
		switch {
		case 'a' <= r && r <= 'z':
			n = n*62 + uint64(r-'a')
		case 'A' <= r && r <= 'Z':
			n = n*62 + uint64(r-'A') + 26
		case '0' <= r && r <= '9':
			n = n*62 + uint64(r-'0') + 52
		}
	}
	return n
}
