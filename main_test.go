package main

import (
	"bytes"
	"io/ioutil"
	"math/rand"
	"net/http"
	"net/http/httptest"
	"net/url"
	"sync"
	"testing"

	"shortener/src/storage"
)

func TestShortener(t *testing.T) {
	if result := Shorten(12345); result != "dnh" {
		t.Errorf("Shorten(%d) must be %s, %s got", 12345, "dnh", result)
	}
}

func TestShortenerFuzzing(t *testing.T) {
	for i := 0; i < 1000; i++ {
		n := rand.Uint64()
		if n != Resolve(Shorten(n)) {
			t.Errorf("Shorten or Resolve broken for (%d) input", n)
		}
	}
}

func TestService(t *testing.T) {
	service := shortener{
		store: storage.NewStorage(),
	}
	server := httptest.NewServer(service)
	fullURL := "https://google.com"
	form := url.Values{}
	form.Add("url", fullURL)
	client := http.Client{
		// prevent client from following redirects
		CheckRedirect: func(req *http.Request, via []*http.Request) error {
			return http.ErrUseLastResponse
		},
	}
	resp, _ := client.PostForm(server.URL, form)
	if resp.StatusCode != 200 {
		t.Errorf("%d != 200", resp.StatusCode)
	}
	responseBytes, _ := ioutil.ReadAll(resp.Body)
	redirect, _ := client.Get(string(responseBytes))
	if redirect.StatusCode != 301 {
		t.Errorf("Unexpected status %d", redirect.StatusCode)
	}
	if redirect.Header.Get("Location") != fullURL {
		t.Errorf("Unexpected redirect location %s", redirect.Header.Get("Location"))
	}
}

func TestRace(t *testing.T) {
	// create few clients and concurrently send the same request to service
	service := shortener{
		store: storage.NewStorage(),
	}
	server := httptest.NewServer(service)
	client := http.Client{
		CheckRedirect: func(req *http.Request, via []*http.Request) error {
			return http.ErrUseLastResponse
		},
	}
	fullURL := "https://google.com"
	form := url.Values{}
	form.Add("url", fullURL)
	var wg sync.WaitGroup
	for i := 0; i < 100; i++ {
		wg.Add(1)
		go func() {
			defer wg.Done()
			req, _ := http.NewRequest("POST", server.URL, bytes.NewBufferString(form.Encode()))
			req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
			req.Close = true
			resp, _ := client.Do(req)
			if resp.StatusCode != 200 {
				t.Errorf("%d != 200", resp.StatusCode)
			}
		}()
	}
	wg.Wait()
}
