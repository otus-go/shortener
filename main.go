package main

import (
	"fmt"
	"log"
	"net/http"
	"net/url"
	"strings"

	"shortener/src/storage"
)

const port = 9000

type shortener struct {
	store storage.Storekeeper // interface
}

func (s shortener) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	// try to fetch record from the storage and make redirect, or return 404
	if r.Method == "GET" {
		base62 := strings.TrimLeft(r.URL.Path, "/")
		urlID := Resolve(base62)
		if url, ok := s.store.Get(urlID); ok {
			http.Redirect(w, r, url, 301)
			return
		}
		w.WriteHeader(http.StatusNotFound)
		return
	}
	// check if url-like value exists in the form
	// and save new record into the storage
	if fullURL := r.PostFormValue("url"); fullURL != "" {
		if _, err := url.ParseRequestURI(fullURL); err != nil {
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		id := s.store.Put(fullURL)
		shortLink := fmt.Sprintf("http://%s/%s", r.Host, Shorten(id))
		fmt.Fprintf(w, shortLink)
		return
	}
	w.WriteHeader(http.StatusBadRequest)
}

func main() {
	shortenerServer := shortener{
		store: storage.NewStorage(),
	}

	err := http.ListenAndServe(fmt.Sprintf(":%d", port), shortenerServer)
	if err != nil {
		log.Fatalf("Could not start server: %s\n", err.Error())
	}
}
