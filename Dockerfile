FROM golang:1.12-alpine as builder

COPY . /outside-gopath
WORKDIR /outside-gopath
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 \
  go build -ldflags="-w -s" -o /go/bin/shortener

FROM scratch
COPY --from=builder /go/bin/shortener /go/bin/shortener
EXPOSE 9000
ENTRYPOINT [ "/go/bin/shortener" ]
