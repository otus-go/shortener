package storage

import (
	"testing"
)


func TestZipper(t *testing.T) {
	str := "Привет, мир!"
	zipped, _ := zip(str)
	if len(zipped) == 0 {
		t.Error("Empty result from zipper")
	}
	unzipped, _ := unzip(zipped)
	if unzipped != str {
		t.Errorf("Compressor or decompressor are broken %s != %s", str, unzipped)
	}
}
