package storage

import (
	"sync"
)

// Storekeeper - simple storage interface
type Storekeeper interface {
	Get(key uint64) (string, bool)
	Put(value string) uint64
}

// Storage - simple key-value storage with autoincremented keys
type Storage struct {
	sync.RWMutex
	Store map[uint64]string
	nextID uint64
}

// Get - get method implementation
func (r *Storage) Get(key uint64) (string, bool) {
	r.RLock()
	defer r.RUnlock()
	value, ok := r.Store[key]
	return value, ok
}

// Put - put method implemetation
func (r *Storage) Put(value string) uint64 {
	r.Lock()
	defer r.Unlock()
	r.nextID++
	insertedID := r.nextID
	r.Store[insertedID] = value
	return insertedID
}

// NewStorage - storage constructor
func NewStorage() *Storage {
	return &Storage{
		Store: make(map[uint64]string),
	}
}
