package storage

import (
	"testing"
)


func TestStorage(t *testing.T) {
	storage := NewStorage()
	storage.Put("Привет")
	storage.Put("Мир")
	value, ok := storage.Get(1)
	if !ok || value != "Привет" {
		t.Error("Doesn't work")
	}
	value, ok = storage.Get(2)
	if !ok || value != "Мир" {
		t.Error("Doesn't work")
	}
}
