package storage

import (
	"compress/gzip"
	"bytes"
	"io/ioutil"
)

func zip(str string) ([]byte, error) {
	var b bytes.Buffer
    gz := gzip.NewWriter(&b)
    if _, err := gz.Write([]byte(str)); err != nil {
        return nil, err
    }
    if err := gz.Flush(); err != nil {
        return nil, err
	}
	// defer gz.Close() doesn't fit here
	// https://stackoverflow.com/questions/19197874/how-can-i-use-gzip-on-a-string-in-golang#comment69100195_19267224
    if err := gz.Close(); err != nil {
        return nil, err
    }
    return b.Bytes(), nil
}

func unzip(zipped []byte) (string, error) {
	buf :=  bytes.NewBuffer(zipped)
	gz, err := gzip.NewReader(buf)
	if err != nil {
		return "", err
	}
	defer gz.Close()
	unzippedBytes, err := ioutil.ReadAll(gz)
	if err != nil {
		return "", err
	}
	return string(unzippedBytes), err
}
