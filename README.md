# Задание

Написать тип, который реализует интерфейс:

```
type Shortener interface {
    Shorten(url string) string
    Resolve(url string) string
}
```

- Shorten - возвращаeт "короткую" ссылку (выбор алгоритма - за студентом), например otus.ru/some-long-link -> otus.ru/jhg34 и сохраняет соответствие короткой и исходной ссылок в памяти (не используя БД, а используя, например, map).
- Resolve - возвращает "длинную ссылку" или пустую строку, если ссылка не найдена.


## Реализация

Реализовывать простой интерфейс как-то совсем неинтересно, поэтому я сделал что-то по мотивам.

Хранилище ключ-значение на основе мапы живет в пакете `./src/storage`. Алгоритм сокращения ссылок лежит в `main`.

Получив запрос, сервис сохраняет url в хранилище, получая от него ключ - целое число. Short url вычисляется переводом этого ключа в base62 строку, которая возвращается пользователю.

## Как запустить

### Без сборки

```
go run main.go shortener.go
```

### В докер-контейнере

```
docker build -t shortener .  # сбилдить образ
docker run --rm -it -p 9000:9000 shortener  # запустить контейнер
```

## Примеры запросов и ответов (для httpie)

### Получить короткую ссылку
```
http --form POST 127.0.0.1:9000 url=https://google.com
HTTP/1.1 200 OK
Content-Length: 23
Content-Type: text/plain; charset=utf-8
Date: Sun, 02 Jun 2019 11:18:03 GMT

http://127.0.0.1:9000/b
```

### Перейти по короткой ссылке

#### Cсылка существует

```
http 127.0.0.1:9000/b
HTTP/1.1 301 Moved Permanently
Content-Length: 53
Content-Type: text/html; charset=utf-8
Date: Sun, 02 Jun 2019 11:20:40 GMT
Location: https://google.com

<a href="https://google.com">Moved Permanently</a>.
```

#### Cсылка не существует

```
http 127.0.0.1:9000/с
HTTP/1.1 404 Not Found
Content-Length: 0
Date: Sun, 02 Jun 2019 11:22:15 GMT
```

## Тесты

Есть некоторое количество тестов, запускаются командой

```bash
go test -v -race ./... 
```
